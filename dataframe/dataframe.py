#!/bin/env python3
__author__ = 'Gao Hao'
__version__ = "0.01"


import numpy as np


class IllegalFormatException(Exception): pass


def split(s, sep=","):
    in_str = False
    values = []
    word = []
    for c in s:
        if c == "," and not in_str:
            values.append("".join(word))
            word.clear()
        elif c == '"':
            in_str = not in_str
        else:
            word.append(c)
    return values


def represent(s, data_type):
    try:
        data_type(s)
        return True
    except ValueError:
        return False


def check_type(values, types):
    lower_type = {int: float, float: str}
    for i in range(0, len(values)):
        if types[i] != str and not represent(values[i], types[i]):
            data_type = lower_type[types[i]]
            while not represent(values[i], data_type):
                data_type = lower_type[data_type]
            types[i] = data_type
    return types


class DataFrame:
    def __init__(self, data):
        """construct a data frame from a list of pair of column name and column values"""
        #TODO check type of data: name should be string, values should have the same dimension and type.
        self._data = {}
        self._names = []
        self._nrow = 0
        for name, values in data:
            self._data[name] = np.array(values)
            self._nrow = values.size
            self._names.append(name)
        self._names = np.array(self._names)

    def head(self, n=5):
        pass

    def nrow(self):
        return self._nrow

    def ncol(self):
        return self._names.size()

    def names(self):
        return self._names

    def col_types(self):
        types = []
        for name in self._names:
            types.append(self._data[name].dtype)
        return np.array(types)

    def __iter__(self):
        #TODO: change it to returning an independent iterator to enable concurrent
        self.cursor = 0
        return self

    def __next__(self):
        """
        a row of the data frame
        return a tuple
        """
        if self.cursor == self.nrow():
            raise StopIteration
        value = []
        for name in self.names():
            value.append(self._data[name][self.cursor])
        self.cursor += 1
        return tuple(value)

    def __str__(self):
        return str(self._data)


def read_csv(path, head=True, sep=","):
    with open(path) as f:
        line = f.readline()
        t = list(map(lambda e: e.strip(), split(line.strip(), sep)))
        ncol = len(t)
        if head:
            header = t
            data = [[] for i in range(0, ncol)]
            types = [int] * ncol
        else:
            header = ["X" + i for i in range(0, ncol)]
            types = check_type(t, [int for i in range(0, ncol)])
            data = [[e] for e in t]

        index = 0
        for line in f.readlines():
            index += 1
            t = list(map(lambda e: e.strip(), split(line.strip())))
            if len(t) != ncol:
                message = "The length of Line {0} should equal the number of columns {1} \n Line Content: {2}"\
                    .format(len(t), ncol, line.rstrip())
                raise IllegalFormatException(message)
            types = check_type(t, types)
            for i in range(0, ncol):
                data[i].append(t[i])

        #change type
        for i in range(0, ncol):
            data[i] = np.array(data[i])
            data_type = types[i]
            if data_type != str:
                data[i] = data[i].astype(data_type)
    return DataFrame(list(zip(header, data)))


def save_csv(data, path, head=True, sep=","):
    pass


def join(d1, d2, type="left", by=None, by_x=None, by_y=None):
    pass


def loose_join(d1, d2, sim_fun, combine_fun, by=None, by_x=None, by_y=None):
    pass
